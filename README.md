## GitLab Omnibus Demo Project

This repository is a demo project to insall GitLab Omnibus Enterprise Edition on an ubuntu server. Infra is deployed on AWS Cloud.

# Terraform
The following resources will be deployed on AWS Cloud by TF.
- VPC with single public subnet & related network resources
- EC2 Instance. Verify the ami base on deployment region and linux os flavour. Note that ansible ssh user in this repo is ubuntu so make adjustments if you are using a different distro
- RSA Keypair

## Ansible
The following task will be executed by ansible.
- Gilab package repository will be added
- Omnibus Enterprise edition will be installed and configured

# Pre-requisites 
- Configure backend. Make sure to configure terraform statefile backend by following the documentation [here](https://docs.gitlab.com/ee/user/infrastructure/iac/terraform_state.html)
- Define your project variables in UI. Required variables for this demo are AWS_ACCESS_KEY, AWS_SECRET_ACCESS_KEY & Gitlab Project Access Token. See documentation [here](https://docs.gitlab.com/ee/ci/variables/#define-a-cicd-variable-in-the-ui:~:text=need%20any%20variables-,Define%20a%20CI/CD%20variable%20in%20the%20UI,-Sensitive%20variables%20like)
